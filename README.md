# A lightweight mirror for Open Food Facts

## How it works

`off-mirror` is a lightweight and fast web server with the equivalent api of openfoodfacts official website. But it need a mongo database filled with [openfoodfacts dump](https://world.openfoodfacts.org/data). `off-mirror` provides a [script](src/refreshdb.js) for downloading, import, shrink and optimize database (but it can take several minutes).

## Features

- Similar api to the official one
    - Fetch one product `/api/v0/product/:product_id.json`
    - Fetch multiples products in a batch `/api/v0/product/batch.json`
- Automatic refresh

## Requirements

- NodeJS and Yarn
- `mongorestore`
- At least 50Go of disk space + 20 Go for the datanase (Database updates are resource-intensive !)

## Startup

1) Install deps
   ```
   yarn install
   ```

2) Create a `config.js` file at the project root and fill it with your informations.
Ex:
    ```js
    module.exports = {
        db: {
            host: 'mongo',
            username: 'root',
            password: 'secret',
        },
        keep_fields: [
            'nutrition_grade_fr', 
            'ingredients_from_palm_oil_n', 
            'ingredients_from_or_that_may_be_from_palm_oil_n',
        ],
        auto_refresh: {
            enabled: false,
        },
    };
    ```

3) (Optional) Import openfoodfacts dump into your database:
   ```
   node refreshdb.js
   ```
   It can take several minutes and use a huge amount of disk space (2-3 times the size of openfoodfacts dump).

   This step is optional, because if you have enabled auto refresh (with `auto_refresh.enabled`), the app will do it automatically.

4) Run the webapp:
   ```
   node bin/www
   ```

## Docker images

You can use docker images from [out registry](https://gitlab.com/bluemanoid/webservices/off-mirror/container_registry).

- registry.gitlab.com/bluemanoid/webservices/off-mirror:latest
