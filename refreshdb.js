const refreshdb = require('./src/refreshdb');
const fs = require('fs');

process.on('unhandledRejection', up => { throw up });  // Crash when there is an error

if (fs.existsSync(refreshdb.getDumpArchivePath())) {
    console.log("Restoring from cache...");
    refreshdb.restoreDumpAsync();
} else {
    refreshdb.refreshDatabaseAsync();
}    
