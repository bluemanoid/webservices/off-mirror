const express = require('express');
const db = require("../../db");
const { query, validationResult } = require('express-validator');
const validators = require('../../validators');

var v0Router = express.Router();

v0Router.get('/product/batch.json', [
    query('ids').isArray().withMessage('must be an array').custom(validators.isArrayLength({ min: 1, max: 100 }))
  ], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    let ids = req.query.ids;

    try {
        const results = await db.get().collection("products").find({ code: { $in: ids } }).toArray();

        let products = new Array(ids.length);
        for (result of results) {
            for (let i = 0; i < ids.length; i++) {
                if (ids[i] === result.code) {
                    products[i] = {
                        status_verbose: "product found",
                        status: 1,
                        code: result.code,
                        product: result,
                    };
                }

                if(products[i] == null) {
                    products[i] = {
                        status_verbose: "product not found",
                        status: 0,
                        code: ids[i],
                    };
                }
            }
        }

        res.json({
            products: products,
        });
    } catch (error) {
        res.status(500).end();
        console.error(error);
    }
});

v0Router.get('/product/:product_id.json', async (req, res) => {
    try {
        const result = await db.get().collection("products").findOne({ code: req.params.product_id });

        if (result) {
            res.json({
                status_verbose: "product found",
                status: 1,
                code: result.code,
                product: result,
            });
        } else {
            res.json({
                status_verbose: "product not found",
                status: 0,
                code: req.params.product_id,
            });
        }
    } catch (error) {
        res.status(500).end();
        console.error(error);
    }
});

module.exports = v0Router;
