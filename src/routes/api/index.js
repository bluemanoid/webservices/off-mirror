const express = require('express');
const apiRouter = express.Router();

apiRouter.use('/v0', require('./v0'));
apiRouter.use('/v2', require('./v2'));

module.exports = apiRouter;
