
function isArrayLength(options) {

    return (arr) => {
        if(!Array.isArray(arr)) {
            let invalidType;
            if (arr === null) {
                invalidType = 'null';
            } else {
                invalidType = typeof arr;
                if (invalidType === 'object' && arr.constructor && arr.constructor.hasOwnProperty('name')) {
                    invalidType = arr.constructor.name;
                } else {
                    invalidType = `a ${invalidType}`;
                }
            }
            throw new TypeError(`Expected array but received ${invalidType}.`);
        }
    
        let min = options.min || 0;
        let max = options.max;

        const len = arr.length;
        if(len >= min && (typeof max === 'undefined' || len <= max)) {
            return true;
        }
        throw new Error(`may not have more than ${max} items.`);
    }
}

module.exports = {
    isArrayLength,
}
