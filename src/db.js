const { MongoClient, Db } = require('mongodb');
const assert = require('assert');

const config = require('./config');

let _client;

/** @type {Db} */
let _db;

/** @return {Db} */
async function init() {
    const dbConfig = config.db;
    const connectionString = `mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.name}`;

    if (_db) {
        console.warn("Trying to init database again!");
        return _db;
    } else {
        _client = await MongoClient.connect(connectionString,  {
            authSource: "admin",
            auth: {
                username: dbConfig.username, 
                password: dbConfig.password, 
            },
        });

        console.log("Database initialized - connected to: " + dbConfig.host);
        _db = _client.db('off');
        return _db;
    }
}

async function close() {
    assert.ok(_client, "Database has not been initialized. Please called init first.");
    await _client.close();

    _client = null;
    _db = null;
}

function get() {
    assert.ok(_db, "Database has not been initialized. Please called init first.");
    return _db;
}

module.exports = {
    init,
    close,
    get
};
